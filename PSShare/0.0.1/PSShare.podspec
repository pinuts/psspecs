Pod::Spec.new do |s|
  s.name         = "PSShare"
  s.version      = "0.0.1"
  s.summary      = "Share with twitter, mail and facebook."
  s.homepage     = "https://bitbucket.org/pinuts/psshare"
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author       = { "Felipe Vieira" => "felipe.vieira@pinutsstudios.com" }
  s.source       = { :git => "https://bitbucket.org/pinuts/psshare.git", :tag => 'v0.0.1' }
  s.platform     = :ios, '5.0'
  s.source_files = 'PSShare'
  s.requires_arc = true
  s.dependency 'Facebook-iOS-SDK'
end